import toastr from "toastr";
import { closeModal } from "./modal.js";

export function joinTeam() {
  const btn = this
  $(btn).attr("disabled", true);

  const userID = $(btn).data("user");
  const teamID = $(btn).data("team");

  const data = new FormData();

  data.append("user_id", userID);
  data.append("team_id", teamID);

  data.append("action", "cyber_rksi_join_team");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === "0") resp = resp.slice(0, -1);
      const response = JSON.parse(resp);

      let errors = false;

      for (let item of response) {
        if (item.status === "error") errors = true;
      }

      if (errors) {
        for (let item of response) {
          if (item.status === "error") toastr.error(item.message);
          else toastr.success(item.message);
        }
      } else {
        location.reload();
      }
    },
    complete: function () {
      $(btn).attr("disabled", false);
    },
  });
}

export function acceptUser() {
  const btn = this
  $(btn).attr("disabled", true);

  const userID = $(btn).data("user");
  const teamID = $(btn).data("team");

  const data = new FormData();

  data.append("user_id", userID);
  data.append("team_id", teamID);

  data.append("action", "cyber_rksi_accept_user");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === "0") resp = resp.slice(0, -1);
      const response = JSON.parse(resp);

      let errors = false;

      for (let item of response) {
        if (item.status === "error") errors = true;
      }

      if (errors) {
        for (let item of response) {
          if (item.status === "error") toastr.error(item.message);
          else toastr.success(item.message);
        }
      } else {
        location.reload();
      }
    },
    complete: function () {
      $(btn).attr("disabled", false);
    },
  });
}

export function rejectUser() {
  const btn = this
  $(btn).attr("disabled", true);

  const userID = $(btn).data("user");
  const teamID = $(btn).data("team");

  const data = new FormData();

  data.append("user_id", userID);
  data.append("team_id", teamID);

  data.append("action", "cyber_rksi_reject_user");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === "0") resp = resp.slice(0, -1);
      const response = JSON.parse(resp);

      let errors = false;

      for (let item of response) {
        if (item.status === "error") errors = true;
      }

      if (errors) {
        for (let item of response) {
          if (item.status === "error") toastr.error(item.message);
          else toastr.success(item.message);
        }
      } else {
        location.reload();
      }
    },
    complete: function () {
      $(btn).attr("disabled", false);
    },
  });
}

export function addTeamUser(data, btn) {
  data.append("action", "cyber_rksi_add_team_user");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === "0") resp = resp.slice(0, -1);
      const response = JSON.parse(resp);

      let errors = false;

      for (let item of response) {
        if (item.status === "error") errors = true;
      }

      for (let item of response) {
        if (item.status === "error") toastr.error(item.message);
        else toastr.success(item.message);
      }

      if (!errors) {
        closeModal();
      }
    },
    complete: function() {
      btn.attr('disabled', false)
    }
  });
}

export function leaveTeam() {
  const btn = this
  $(btn).attr("disabled", true);

  const userID = $(btn).data("user");
  const teamID = $(btn).data("team");

  const data = new FormData();

  data.append("user_id", userID);
  data.append("team_id", teamID);

  data.append("action", "cyber_rksi_leave_team");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === "0") resp = resp.slice(0, -1);
      const response = JSON.parse(resp);

      let errors = false;

      for (let item of response) {
        if (item.status === "error") errors = true;
      }

      if (errors) {
        for (let item of response) {
          if (item.status === "error") toastr.error(item.message);
          else toastr.success(item.message);
        }
      } else {
        location = response[0]["url"];
      }
    },
    complete: function() {
      $(btn).attr('disabled', false)
    }
  });
}

export function excludeUser() {
  const btn = this
  $(btn).attr("disabled", true);

  const userID = $(btn).data("user");
  const teamID = $(btn).data("team");

  const data = new FormData();

  data.append("user_id", userID);
  data.append("team_id", teamID);

  data.append("action", "cyber_rksi_exclude_user");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === "0") resp = resp.slice(0, -1);
      const response = JSON.parse(resp);

      let errors = false;

      for (let item of response) {
        if (item.status === "error") errors = true;
      }

      if (errors) {
        for (let item of response) {
          if (item.status === "error") toastr.error(item.message);
          else toastr.success(item.message);
        }
      } else {
        location.reload();
      }
    },
    complete: function() {
      $(btn).attr('disabled', false)
    }
  });
}

export function createTeam(data, btn) {
  data.append("action", "cyber_rksi_create_team");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === "0") resp = resp.slice(0, -1);
      const response = JSON.parse(resp);

      let errors = false;

      for (let item of response) {
        if (item.status === "error") errors = true;
      }

      if (errors) {
        for (let item of response) {
          if (item.status === "error") toastr.error(item.message);
          else toastr.success(item.message);
        }
      } else {
        location = response[0]["url"];
      }
    },
    complete: function() {
      btn.attr('disabled', false)
    }
  });
}

export function updateTeam(data, btn) {
  data.append("action", "cyber_rksi_update_team");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === "0") resp = resp.slice(0, -1);
      const response = JSON.parse(resp);

      let errors = false;

      for (let item of response) {
        if (item.status === "error") errors = true;
      }

      if (errors) {
        for (let item of response) {
          if (item.status === "error") toastr.error(item.message);
          else toastr.success(item.message);
        }
      } else {
        location.reload();
      }
    },
    complete: function() {
      btn.attr('disabled', false)
    }
  });
}
