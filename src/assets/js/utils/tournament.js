import toastr from "toastr";

export function singleTournament(btn) {
  $(btn).attr("disabled", true);

  const userID = $(btn).data('user')
  const tournamentID = $(btn).data('tournament')

  const data = new FormData()
  
  data.append('user_id', userID)
  data.append('tournament_id', tournamentID) 

  data.append("action", "cyber_rksi_single_tournament");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === '0') resp = resp.slice(0, -1)
      const response = JSON.parse(resp);
      
      let errors = false;

      for (let item of response) {
        if (item.status === 'error') errors = true
      }

      if (errors) {
        for (let item of response) {
          if (item.status === 'error') toastr.error(item.message)
          else toastr.success(item.message)
        }
      } else {
        location.reload()
      }
    },
    complete: function () {
      $(btn).attr("disabled", false);
    },
  });
}

export function teamTournament(btn) {
  $(btn).attr("disabled", true);

  const teamID = $(btn).data('team')
  const tournamentID = $(btn).data('tournament')

  const data = new FormData()
  
  data.append('team_id', teamID)
  data.append('tournament_id', tournamentID)

  data.append("action", "cyber_rksi_team_tournament");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === '0') resp = resp.slice(0, -1)
      const response = JSON.parse(resp);
      
      let errors = false;

      for (let item of response) {
        if (item.status === 'error') errors = true
      }

      if (errors) {
        for (let item of response) {
          if (item.status === 'error') toastr.error(item.message)
          else toastr.success(item.message)
        }
      } else {
        location.reload()
      }
    },
    complete: function () {
      $(btn).attr("disabled", false);
    },
  });
}