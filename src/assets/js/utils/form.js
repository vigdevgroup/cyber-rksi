export function checkInputValid(target) {
  let type = target.type;
  if (target instanceof NodeList) {
    type = target[0].type;
  }
  let valid = false;
  let regexp;

  switch (type) {
    case "text":
      if (target.dataset.onlywords)
        regexp = /^[a-zA-Zа-яА-Я][a-zA-Zа-яА-Я\s]+$/;
      else regexp = /^.+$/;
      if (regexp.test(target.value.trim())) valid = true;
      break;
    case "email":
      regexp = /^[\w-.]+@([-\w]+\.)+[-\w]+$/;
      if (regexp.test(target.value.trim())) valid = true;
      break;
    case "textarea":
      if (target.value.trim().length > 5) valid = true;
      break;
    case "tel":
      if (target.value.trim().length === 18) valid = true;
      break;
    case "radio":
      if (target.value) valid = true;
      break;
    case "checkbox":
      if (target.checked) valid = true;
      break;
    case "select-one":
      if (target.value.trim()) valid = true;
      break;
    case "password":
      if (target.value.trim()) valid = true;
      break;
    case "hidden":
      valid = true;
      break;
    case 'file':
      if (target.files.length) valid = true
      break;
  }

  return valid;
}

export function checkInputEmpty(target) {
  let type = target.type;
  if (target instanceof NodeList) {
    type = target[0].type;
  }
  let empty = false;

  switch (type) {
    case "checkbox":
      if (!target.checked) empty = true;
      break;
    case 'file':
      if (!target.files.length) empty = true;
      break;
    default:
      if (!target.value) empty = true;
      break;
  }

  return empty;
}

export function checkInput(target) {
  let field = target;
  if (target instanceof NodeList) {
    field = target[0];
  }

  if (checkInputValid(target)) {
    field.closest(".input-wrapper").classList.remove("_error");
    field.closest(".input-wrapper").classList.add("_filled");
    return true;
  } else if (checkInputEmpty(target)) {
    field.closest(".input-wrapper").classList.remove("_error");
    field.closest(".input-wrapper").classList.remove("_filled");
    return false;
  } else {
    field.closest(".input-wrapper").classList.add("_filled");
    field.closest(".input-wrapper").classList.add("_error");
    return false;
  }
}

export function checkForm(form) {
  let errs = false;

  let data = new FormData();
  let formEls = $(form).serializeArray();
  $(form)
    .find('input[type="radio"], input[type="checkbox"]')
    .each(function () {
      const name = this.name;
      if (!formEls.find((el) => el.name === name)) {
        formEls.push({ name, value: "" });
      }
    });

  $(form)
    .find('input[type="file"]')
    .each(function () {
      const name = this.name;
      let target = form[this.name];
      const value = target.files[0];
      if (!formEls.find((el) => el.name === name)) {
        formEls.push({ name, value });
      }
    });

  $.each(formEls, function () {
    let target = form[this.name];
    let field = target;
    if (target instanceof NodeList) {
      field = target[0];
    }

    if (target.dataset.required !== "true" && checkInputEmpty(target)) {
      field.closest(".input-wrapper").classList.remove("_error");
    } else if (
      !checkInputValid(target) ||
      (target.dataset.required === "true" && checkInputEmpty(target))
    ) {
      errs = true;
      field.closest(".input-wrapper").classList.add("_error");
    } else {
      field.closest(".input-wrapper").classList.remove("_error");
    }
  });

  if (errs) return { valid: false, data, formEls };

  $.each(formEls, function () {
    let value = this.value
    if (typeof value === 'string') value = value.trim()
    data.append(this.name, value);
  });

  return { valid: true, data, formEls };
}

export function clearForm(form, formEls) {
  $.each(formEls, function () {
    const target = form[this.name];
    let type = target.type;
    if (target instanceof NodeList) {
      type = target[0].type;
    }

    if (["text", "email", "textarea", "tel", "file"].includes(type)) {
      target.value = "";
    }
  });
}
