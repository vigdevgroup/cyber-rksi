export function openModal(module) {
  const modal = $(".modal");
  const moduleEl = $(`.${module}-module`);
  modal.find(".modal__inner").html(moduleEl.html());

  modal.addClass("_show");

  modal.find('.jq-field').each(function () {
    $(this).styler({
      selectSmartPositioning: false
    })
  })

  setTimeout(() => {
    modal.addClass("_visible");
    $("body").addClass("_noscroll");
    document.addEventListener("click", closeModalHandler);
    document.addEventListener("keyup", closeModalHandler);
  }, 0);
}

export function changeModal(module) {
  const modal = $(".modal");
  const inner = $(modal).find(".modal__inner");
  const moduleEl = $(`.${module}-module`);

  inner.addClass("_hide");
  setTimeout(() => {
    inner.html(moduleEl.html())
    inner.removeClass("_hide");
  }, 210);
}

function closeModalHandler(event) {
  let condition = false;
  if (event.code && event.code === "Escape") condition = true;
  if (event.target && event.target.closest('.modal__close')) condition = true
  if (event.target && event.target.classList.contains("modal__inner"))
    condition = true;

  if (condition) {
    closeModal()
  }
}

export function closeModal(event) {
  const modal = $(".modal");

  modal.removeClass("_visible");
  document.removeEventListener("click", closeModalHandler);
  document.removeEventListener("keyup", closeModalHandler);
  $("body").removeClass("_noscroll");

  setTimeout(() => {
    modal.find('.jq-field').each(function () {
      $(this).styler('destroy')
    })

    modal.removeClass("_show");
  }, 310);
}