import toastr from "toastr"

export function acceptInvitation() {
  const btn = this
  $(btn).attr("disabled", true);

  const userID = $(btn).data('user')
  const teamID = $(btn).data('team')
 
  const data = new FormData()
  
  data.append('user_id', userID)
  data.append('team_id', teamID)

  data.append("action", "cyber_rksi_accept_invitation");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === '0') resp = resp.slice(0, -1)
      const response = JSON.parse(resp);
      
      let errors = false;

      for (let item of response) {
        if (item.status === 'error') errors = true
      }

      if (errors) {
        for (let item of response) {
          if (item.status === 'error') toastr.error(item.message)
          else toastr.success(item.message)
        }
      } else {
        location.reload()
      }
    },
    complete: function () {
      $(btn).attr("disabled", false);
    },
  });
}

export function rejectInvitation() {
  const btn = this
  $(btn).attr("disabled", true);

  const userID = $(btn).data('user')
  const teamID = $(btn).data('team')

  const data = new FormData()
  
  data.append('user_id', userID)
  data.append('team_id', teamID)

  data.append("action", "cyber_rksi_reject_invitation");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === '0') resp = resp.slice(0, -1)
      const response = JSON.parse(resp);
      
      let errors = false;

      for (let item of response) {
        if (item.status === 'error') errors = true
      }

      if (errors) {
        for (let item of response) {
          if (item.status === 'error') toastr.error(item.message)
          else toastr.success(item.message)
        }
      } else {
        location.reload()
      }
    },
    complete: function () {
      $(btn).attr("disabled", false);
    },
  });
}

export function updateUser(data, btn) {
  data.append("action", "cyber_rksi_update_user");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false, 
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === '0') resp = resp.slice(0, -1)
      const response = JSON.parse(resp);

      let errors = false;

      for (let item of response) {
        if (item.status === 'error') errors = true
      }

      if (errors) {
        for (let item of response) {
          if (item.status === 'error') toastr.error(item.message)
          else toastr.success(item.message)
        }
      } else {
        location.reload()
      }
    },
    complete: function() {
      btn.attr('disabled', false)
    }
  });
}