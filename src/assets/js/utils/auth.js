import {changeModal} from './modal.js'
import toastr from 'toastr';

export function auth(data, btn) {
  data.append("action", "cyber_rksi_login");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === '0') resp = resp.slice(0, -1)
      const response = JSON.parse(resp);

      let errors = false;

      for (let item of response) {
        if (item.status === 'error') errors = true
      }

      if (errors) {
        for (let item of response) {
          if (item.status === 'error') toastr.error(item.message)
          else toastr.success(item.message)
        }
      } else {
        location = response[0]['url']
      }
    },
    complete: function() {
      btn.attr('disabled', false)
    }
  });
}

export function register(data, btn) {
  data.append("action", "cyber_rksi_register");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function (resp) {
      if (resp.slice(-1) === '0') resp = resp.slice(0, -1)
      const response = JSON.parse(resp);

      let errors = false;

      for (let item of response) {
        if (item.status === 'error') errors = true
      }

      if (errors) {
        for (let item of response) {
          if (item.status === 'error') toastr.error(item.message)
          else toastr.success(item.message)
        }
      } else {
        changeModal('auth')
      }
    },
    complete: function() {
      btn.attr('disabled', false)
    }
  });
}

export function logout() {
  const btn = this
  $(btn).attr('disabled', true)

  const data = new FormData()
  data.append("action", "cyber_rksi_logout");
  data.append("nonce", wp_vars.nonce);

  $.ajax({
    url: wp_vars.url,
    data: data,
    method: "POST",
    contentType: false,
    processData: false,
    dataType: "text",
    success: function(resp) {
      if (resp.slice(-1) === '0') resp = resp.slice(0, -1)
      const response = JSON.parse(resp);

      let errors = false;

      for (let item of response) {
        if (item.status === 'error') errors = true
      }

      if (errors) {
        for (let item of response) {
          if (item.status === 'error') toastr.error(item.message)
          else toastr.success(item.message)
        }
      } else {
        location = response[0]['url']
      }
    },
    complete: function() {
      $(btn).attr('disabled', false)
    }
  });
}
