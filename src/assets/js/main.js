import Swiper from "swiper/bundle";
import toastr from "toastr";
import { checkInput, checkForm } from "./utils/form.js";
import { openModal } from "./utils/modal.js";
import { auth, logout, register } from "./utils/auth.js";
import {
  acceptUser,
  addTeamUser,
  joinTeam,
  rejectUser,
  leaveTeam,
  excludeUser,
  createTeam,
  updateTeam,
} from "./utils/team.js";
import {
  acceptInvitation,
  rejectInvitation,
  updateUser,
} from "./utils/user.js";
import { singleTournament, teamTournament } from "./utils/tournament.js";
import "jquery-form-styler/dist/jquery.formstyler.js";

$(function () {
  $(".preloader").addClass("_hidden");
  $("body").removeClass("_noscroll");

  // TOAST

  toastr.options.toastClass = "toast-modal";
  toastr.options.closeDuration = 200;
  toastr.options.hideDuration = 200;
  toastr.options.showDuration = 200;
  toastr.options.showEasing = "swing";
  toastr.options.hideEasing = "swing";
  toastr.options.closeEasing = "swing";

  // SLIDERS

  new Swiper(".main__slider", {
    effect: "fade",
    loop: true,
    autoplay: {
      delay: 4000,
    },
    pagination: {
      el: ".main__slider-pagination",
      type: "bullets",
    },
  });

  new Swiper(".section__slider", {
    spaceBetween: 24,
    slidesPerView: "auto",
    freeMode: true,
    grabCursor: true,
    breakpoints: {
      501: {
        spaceBetween: 32,
      },
      1025: {
        spaceBetween: 45,
      },
    },
  });

  new Swiper(".profile__nav", {
    spaceBetween: 24,
    slidesPerView: "auto",
    freeMode: true,
    watchOverflow: true,
    breakpoints: {
      501: {
        spaceBetween: 32,
      },
      769: {
        spaceBetween: 48,
      },
    },
  });

  // INFO SIDEBAR

  $(".sidebar__close").on("click", function () {
    if ($(".sidebar").hasClass("_opened")) {
      closeInfoSidebar();
    } else {
      $(".sidebar").addClass("_opened");
      document.addEventListener("click", closeHandlerInfoSidebar);
    }
  });

  function closeHandlerInfoSidebar(event) {
    if (event.target.closest(".sidebar")) return;
    closeInfoSidebar();
  }

  function closeInfoSidebar() {
    $(".sidebar").removeClass("_opened");
    document.removeEventListener("click", closeHandlerInfoSidebar);
  }

  // Mobile menu

  $(".header__mobile-btn").on("click", function () {
    if ($(".header").hasClass("_menu")) {
      $(".header").removeClass("_menu");
      $(".header__menu").fadeOut(200);
    } else {
      $(".header").addClass("_menu");
      $(".header__menu").fadeIn(200);
    }
  });

  // MODAL

  $(".btn-modal").on("click", function () {
    openModal($(this).data("target"));
  });

  // FORM

  $("input").each(function () {
    if ($(this).val()) {
      this.closest(".input-wrapper").classList.add("_filled");
    }
  });

  $("body").on("change", function (event) {
    checkInputHandler(event.target);
  });

  function checkInputHandler(target) {
    const form = target.closest("form");
    if (form && form.dataset.checking == "false") return;

    checkInput(target);
  }

  $(document).on("change", function (event) {
    const target = event.target;

    if (!$(target).hasClass("file-avatar__input")) return;

    if (!target.files || !target.files[0]) return;

    if (target.files[0].type !== "image/jpeg") return;

    const avatar = target.closest(".file-avatar");

    const reader = new FileReader();
    reader.onload = function (e) {
      let preview = $(avatar).find(".file-avatar__img-preview").get(0);

      if (!preview) {
        preview = document.createElement("img");
        $(preview).addClass("file-avatar__img-preview");
        $(avatar).find(".file-avatar__img-game").remove();
        $(avatar).find(".file-avatar__img").append(preview);
      }

      $(preview).attr("src", e.target.result);
    };
    reader.readAsDataURL(target.files[0]);
  });

  // Auth

  $(document).on("submit", authForm);
  function authForm(event) {
    const form = event.target;

    if (!$(form).hasClass("auth-form")) return;
    event.preventDefault();

    const { valid, data, formEls } = checkForm(form);

    if (!valid) return;

    const btn = $(form).find('button[type="submit"]')
    btn.attr('disabled', true)    

    auth(data, btn);
  }

  $(document).on("submit", registerForm);
  function registerForm(event) {
    const form = event.target;

    if (!$(form).hasClass("register-form")) return;
    event.preventDefault();

    const { valid, data, formEls } = checkForm(form);

    if (!valid) return;

    const btn = $(form).find('button[type="submit"]')
    btn.attr('disabled', true) 

    register(data, btn);
  }

  $(".btn-exit").on("click", logout);

  // Team

  $(".btn-join-team").on("click", joinTeam);
  $(".btn-accept-user").on("click", acceptUser);
  $(".btn-reject-user").on("click", rejectUser);
  $(".btn-leave-team").on("click", leaveTeam);
  $(".btn-exclude-user").on("click", excludeUser);

  $(document).on("submit", function (event) {
    const form = event.target;

    if (!$(form).hasClass("add-team-user-form")) return;
    event.preventDefault();

    const { valid, data, formEls } = checkForm(form);

    if (!valid) return;

    const btn = $(form).find('button[type="submit"]')
    btn.attr('disabled', true) 

    addTeamUser(data, btn);
  });

  $(document).on("submit", function (event) {
    const form = event.target;

    if (!$(form).hasClass("create-team-form")) return;
    event.preventDefault();

    const { valid, data, formEls } = checkForm(form);

    if (!valid) return;

    const btn = $(form).find('button[type="submit"]')
    btn.attr('disabled', true) 

    createTeam(data, btn);
  });

  $(document).on("submit", function (event) {
    const form = event.target;

    if (!$(form).hasClass("update-team-form")) return;
    event.preventDefault();

    const { valid, data, formEls } = checkForm(form);

    if (!valid) return;

    const btn = $(form).find('button[type="submit"]')
    btn.attr('disabled', true) 

    updateTeam(data, btn);
  });

  // User

  $(".btn-accept-invitation").on("click", acceptInvitation);
  $(".btn-reject-invitation").on("click", rejectInvitation);

  $(document).on("submit", function (event) {
    const form = event.target;

    if (!$(form).hasClass("update-user-form")) return;
    event.preventDefault();

    const { valid, data, formEls } = checkForm(form);

    if (!valid) return;

    const btn = $(form).find('button[type="submit"]')
    btn.attr('disabled', true) 

    updateUser(data, btn);
  });

  // Tournament

  $(document).on("click", function (event) {
    const target = event.target.closest(".btn-single-tournament");
    if (!target) return;
    singleTournament(target);
  });

  $(document).on("click", function (event) {
    const target = event.target.closest(".btn-team-tournament");
    if (!target) return;
    teamTournament(target);
  });
});
